import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { map } from 'rxjs/operators';
import get from 'lodash/get';

import * as Config from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class WordpressService {

  constructor(private http: HttpClient) { }

  private baseURL = Config.WORDPRESS_URL;

  fetchPages() {
    return this.http
      .get(`${this.baseURL}/wp-json/wp/v2/pages?_embed`)
      .pipe(
        map((pages: Array<any>) => pages.map(this.setEmbeddedFeaturedImage))
      );
  }

  fetchPage(pages_id: string) {
    return this.http
      .get(`${this.baseURL}/wp-json/wp/v2/pages/${pages_id}?_embed`)
      .pipe(map((pages: any) => this.setEmbeddedFeaturedImage(pages)));
  }

  fetchPagesByCategory(category_id: string) {
    return this.http
      .get(
        `${this.baseURL}/wp-json/wp/v2/pages?_embed&categories=${category_id}`
      )
      .pipe(
        map((pages: Array<any>) => pages.map(this.setEmbeddedFeaturedImage))
      );
  }

  /**
   * Makes the featured image parameter easily accessible in a template
   */
  private setEmbeddedFeaturedImage(p) {
    return Object.assign({}, p, {
      featured_image: get(p, "_embedded['wp:featuredmedia'][0].source_url")
    });
  }
}
