import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { map } from 'rxjs/operators';
import get from 'lodash/get';

import * as Config from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class RutasService {

 rutas: any;

  constructor(private http: HttpClient) { }

  private baseURL = Config.WORDPRESS_URL;

  fetchRutas() {
      return this.http
      .get(`${this.baseURL}/wp-json/wp/v2/ruta`)
      .pipe(
        map((rutas: Array<any>) => rutas.map(this.setEmbeddedFeaturedImage)),
      );

  }


  fetchRuta(rutas_id: string) {
    return this.http
      .get(`${this.baseURL}/wp-json/wp/v2/ruta/${rutas_id}`)
      .pipe(map((rutas: any) => this.setEmbeddedFeaturedImage(rutas)));
  }

  fetchRutasByCategory(category_id: string) {
    return this.http
      .get(
        `${this.baseURL}/wp-json/wp/v2/ruta?_embed&categories=${category_id}`
      )
      .pipe(
        map((rutas: Array<any>) => rutas.map(this.setEmbeddedFeaturedImage))
      );
  }

  /**
   * Makes the featured image parameter easily accessible in a template
   */
  private setEmbeddedFeaturedImage(p) {
    return Object.assign({}, p, {
      featured_image: get(p, "_embedded['wp:featuredmedia'][0].source_url")
    });
  }
}
