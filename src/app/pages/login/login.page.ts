import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, LoadingController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { WordpressService } from '../../services/wordpress.service';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'page-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {
  login_form: FormGroup;
  error_message: string;

  userToken;
  displayname: string;
  email: string;

  constructor(public navCtrl: NavController,
              public loadingCtrl: LoadingController,
              public formBuilder: FormBuilder,
              public wordpressService: WordpressService,
              public authenticationService: AuthenticationService,
              private route: ActivatedRoute,
              public router: Router
            ) {

              this.login_form = formBuilder.group({
                username: ['', Validators.compose([Validators.required])],
                password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
              });

            }

  ionViewWillLoad() {
  /*  this.login_form = this.formBuilder.group({
      username: new FormControl('', Validators.compose([
        Validators.required
      ])),
      password: new FormControl('', Validators.required)
    });
    */
  }

  async login(value) {
    let loading = await this.loadingCtrl.create();
    loading.present();

    this.authenticationService.doLogin(value.username, value.password)
    .subscribe(res => {

      this.userToken = res;

      console.log(value.password);

       this.authenticationService.setUser({
         token: this.userToken.token,
         username: value.username,
         displayname: this.userToken.user_display_name,
         email: this.userToken.user_email,
         password: value.password
       });

       loading.dismiss();
       this.router.navigateByUrl('/home');
     },
     err => {
       loading.dismiss();
       this.error_message = "El usuario o la contraseña no son correctos";
       console.log(err);
     });
  }

  skipLogin() {
    this.router.navigateByUrl('/home');
  }

  goToRegister() {
    this.router.navigateByUrl('/register');
  }

}