import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { WordpressService } from '../../services/wordpress.service';

@Component({
  selector: 'app-page',
  templateUrl: './page.page.html',
  styleUrls: ['./page.page.scss'],
})
export class PagePage implements OnInit {

  constructor(private route: ActivatedRoute, private postSrvc: WordpressService) { }

  page$: Observable<any>;
  ngOnInit() {
    this.page$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => this.postSrvc.fetchPage(params.get('id')))
    );
  }

}
