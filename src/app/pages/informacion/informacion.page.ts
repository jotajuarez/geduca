import { WordpressService } from '../../services/wordpress.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';


@Component({
  selector: 'app-informacion',
  templateUrl: './informacion.page.html',
  styleUrls: ['./informacion.page.scss'],
})
export class InformacionPage implements OnInit {

  constructor(private postSrvc: WordpressService,
    private router: Router,
    private route: ActivatedRoute) { }

  pages$: Observable<any>;
  loadPage(page: any) {
    this.router.navigate(['/page', page.id]);
  }

  ngOnInit() {
    this.pages$ = this.route.paramMap.pipe(
      switchMap(
        (params: ParamMap) =>
          params.get('category')
            ? this.postSrvc.fetchPagesByCategory(params.get('category'))
            : this.postSrvc.fetchPages()
      )
    );
  }

}
