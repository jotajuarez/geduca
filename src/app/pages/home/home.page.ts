import { PostsService } from '../../services/posts.service';
import { RutasService } from '../../services/rutas.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { AuthenticationService } from '../../services/authentication.service';

import { NativeStorage } from '@ionic-native/native-storage/ngx';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage implements OnInit {

  morePagesAvailable: boolean = true;
  loggedUser: boolean = false;

  Usuario: any;

  constructor(private postSrvc: PostsService,
              private rutaSrvc: RutasService,
              private router: Router,
              private route: ActivatedRoute,
              public authenticationService: AuthenticationService,
              public nativeStorage: NativeStorage ) {

                this.recuperarUsuario();

              }

  posts$: Observable<any>;
  loadPost(post: any) {
    this.router.navigate(['/posts', post.id]);
  }

  rutas$: Observable<any>;

  loadRuta(ruta: any) {
    this.router.navigate(['/ruta', ruta.id]);
  }

  ionViewWillEnter() {
    this.authenticationService.getUser()
    .then(
      data => this.loggedUser = true,
      error => this.loggedUser = false
    );
    this.morePagesAvailable = true;


  }


  ngOnInit() {
    this.posts$ = this.route.paramMap.pipe(
      switchMap(
        (params: ParamMap) =>
          params.get('category')
            ? this.postSrvc.fetchPostsByCategory(params.get('category'))
            : this.postSrvc.fetchPosts()
      )
    );

    this.rutas$ = this.route.paramMap.pipe(
      switchMap(
        (params: ParamMap) =>
          params.get('category')
            ? this.rutaSrvc.fetchRutasByCategory(params.get('category'))
            : this.rutaSrvc.fetchRutas()
      )
    );
  }

  logOut() {
    this.authenticationService.logOut()
    .then(
      res => this.router.navigateByUrl('/login'),
      err => console.log('Error in log out')
    );
  }

  goToLogin() {
    this.router.navigateByUrl('/login');
  }

  recuperarUsuario() {

    this.nativeStorage.getItem('User')
      .then(data => {
          console.log(data);
          this.Usuario = data.username;
          console.log(this.Usuario);
       },
        error => console.error(error)
      );
  }

}
