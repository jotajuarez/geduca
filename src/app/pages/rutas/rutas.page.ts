import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { RutasService } from '../../services/rutas.service';

import * as Config from '../../config/config';

import { HttpClient } from '@angular/common/http';

import {
  ToastController,
  LoadingController
} from '@ionic/angular';
import {
  GoogleMap,
} from '@ionic-native/google-maps';


@Component({
  selector: 'app-rutas',
  templateUrl: './rutas.page.html',
  styleUrls: ['./rutas.page.scss'],
})
export class RutasPage implements OnInit {

  map: GoogleMap;
  loading: any;
  private baseURL = Config.WORDPRESS_URL;
  ruta$: Observable<any>;
  rutas: any;

  constructor(private route: ActivatedRoute,
              private postSrvc: RutasService,
              public loadingCtrl: LoadingController,
              public toastCtrl: ToastController,
              private http: HttpClient) {

              this.reciboDatos();
              }

  ngOnInit() {
    this.ruta$ = this.postSrvc.fetchRutas();
  }

  getGoogleMaps(apiKey: string): Promise<any> {
    const win = window as any;
    const googleModule = win.google;
    if (googleModule && googleModule.maps) {
      return Promise.resolve(googleModule.maps);
    }

    return new Promise((resolve, reject) => {
      const script = document.createElement('script');
      script.src = `https://maps.googleapis.com/maps/api/js?key=${apiKey}&v=3.31`;
      script.async = true;
      script.defer = true;
      document.body.appendChild(script);
      script.onload = () => {
        const googleModule2 = win.google;
        if (googleModule2 && googleModule2.maps) {
          resolve(googleModule2.maps);
        } else {
          reject('google maps not available');
        }
      };
    });
  }

 async reciboDatos() {
 // la costante de la api //
 const googleMaps = await this.getGoogleMaps(
  'AIzaSyCcCg37YNttJ-i6nxKLFkh-nSCP-O8pZRY'
);

    const url = this.baseURL + "/wp-json/wp/v2/ruta";
    this.http.get( url )
            .subscribe( data => {
                this.rutas = data;
                console.log(this.rutas);

                /*Comienza los mapas*/

                  // Creating a new map
                  const myLatlng = {lat: JSON.parse(this.rutas[0].lat), lng: JSON.parse(this.rutas[0].lon)};

                  this.map = new googleMaps.Map(document.getElementById('mapCanvas'), {
                    center: myLatlng,
                    zoom: 8
                  });
                  /*bucle para los pings*/

                  let i;

                  for (i = 0; i < this.rutas.length; i++) {
                    // recupero las rutas y las asigno a los pings
                    const pings = this.rutas[i];
                    // generamos la latitud y la longitud con lo recibido de la api
                    const latLng = new googleMaps.LatLng(JSON.parse(pings.lat), JSON.parse(pings.lon));

                    // creamos las marcas en el mapa
                    const marker = new googleMaps.Marker({
                      position: latLng,
                      map: this.map,
                      title: pings.info
                    });

                    const infoWindow = new googleMaps.InfoWindow();

                    // Attaching a click event to the current marker
                    googleMaps.event.addListener(marker, "click", function(e) {
                      infoWindow.setContent(pings.info);
                      infoWindow.open(this.map, marker);
                    });

                  }




    });


  }



}