import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SitiosPage } from './sitios.page';

describe('SitiosPage', () => {
  let component: SitiosPage;
  let fixture: ComponentFixture<SitiosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SitiosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SitiosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
