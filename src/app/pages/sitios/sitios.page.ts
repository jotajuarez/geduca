import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import * as Config from '../../config/config';
import { PostsService } from '../../services/posts.service';
import { AuthenticationService } from '../../services/authentication.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { NativeStorage } from '@ionic-native/native-storage/ngx';

@Component({
  selector: 'app-sitios',
  templateUrl: './sitios.page.html',
  styleUrls: ['./sitios.page.scss'],
})
export class SitiosPage implements OnInit {

  register_ruta: FormGroup;
  userToken;
  Usuario: any;
  token;
  password;

  constructor(public navCtrl: NavController,
              public formBuilder: FormBuilder,
              public wordpressService: PostsService,
              public authenticationService: AuthenticationService,
              public http: HttpClient,
              private route: ActivatedRoute,
              public router: Router,
              public nativeStorage: NativeStorage) {

                this.register_ruta = formBuilder.group({
                  title: ['', Validators.compose([Validators.required])],
                  salida: ['', Validators.compose([Validators.required])],
                  destino: ['', Validators.compose([Validators.required])]
                });

               }

  ngOnInit() {

    console.log(this.token);
  }


  onSubmit(values) {

    this.nativeStorage.getItem('User')
      .then(data => {
          console.log(data);
          this.Usuario = data.username;
          this.token = data.token;
          const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa( data.username + ':' + data.password)
          });

       console.log(headers);

    return this.http.post(Config.WORDPRESS_REST_API_RUTAS , values, {headers: headers})
    .subscribe(
      res => {
        const user_data = {
          title: values.title,
          status: "publish",
          salida: values.salida,
          destino: values.destino,
          autor: this.Usuario,
        };
        console.log(user_data);

        this.router.navigateByUrl('/rutas');
       });

       },
        error => console.error(error)
      );




      }


}


