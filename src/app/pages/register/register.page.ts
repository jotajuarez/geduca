
import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import * as Config from '../../config/config';
import { PostsService } from '../../services/posts.service';
import { AuthenticationService } from '../../services/authentication.service';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage {

  register_form: FormGroup;
  userToken;

  constructor(public navCtrl: NavController,
              public formBuilder: FormBuilder,
              public wordpressService: PostsService,
              public authenticationService: AuthenticationService,
              public http: HttpClient,
              private route: ActivatedRoute,
              public router: Router
            ) {

    this.register_form = formBuilder.group({
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      displayName: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required, Validators.email])]
    });
  }

  ionViewWillLoad() {
  }


onSubmit(values) {

  return this.http.post(Config.WORDPRESS_REST_API_USER, values)
  .subscribe(
    res => {
      const user_data = {
        username: values.username,
        first_name: values.displayName,
        email: values.email,
        password: values.password
      };
      console.log(res);
      this.router.navigateByUrl('/login');
     });

    }

}