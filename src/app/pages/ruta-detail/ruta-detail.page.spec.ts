import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RutaDetailPage } from './ruta-detail.page';

describe('RutaDetailPage', () => {
  let component: RutaDetailPage;
  let fixture: ComponentFixture<RutaDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RutaDetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RutaDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
