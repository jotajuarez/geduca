import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { RutasService } from '../../services/rutas.service';

@Component({
  selector: 'app-ruta-detail',
  templateUrl: './ruta-detail.page.html',
  styleUrls: ['./ruta-detail.page.scss'],
})
export class RutaDetailPage implements OnInit {
  constructor(private route: ActivatedRoute, private postSrvc: RutasService) {}
  ruta$: Observable<any>;
  ngOnInit() {
    this.ruta$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => this.postSrvc.fetchRuta(params.get('id')))
    );
  }
}

