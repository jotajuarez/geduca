import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './pages/home/home.module#HomePageModule'
  },
  {
    path: 'home/:category',
    loadChildren: './pages/home/home.module#HomePageModule'
  },
  {
    path: 'posts/:id',
    loadChildren: './pages/post-detail/post-detail.module#PostDetailPageModule'
  },
  {
    path: 'categories',
    loadChildren: './pages/categories/categories.module#CategoriesPageModule'
  },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule' },
  { path: 'informacion', loadChildren: './pages/informacion/informacion.module#InformacionPageModule' },
  { path: 'page/:id', loadChildren: './pages/page/page.module#PagePageModule' },
  { path: 'rutas', loadChildren: './pages/rutas/rutas.module#RutasPageModule' },
  { path: 'ruta/:id', loadChildren: './pages/ruta-detail/ruta-detail.module#RutaDetailPageModule' },
  { path: 'mapas', loadChildren: './pages/mapas/mapas.module#MapasPageModule' },
  { path: 'sitios', loadChildren: './pages/sitios/sitios.module#SitiosPageModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
