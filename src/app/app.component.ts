import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ActivatedRoute, Router } from '@angular/router';

import { AuthenticationService } from './services/authentication.service';

import { Environment } from '@ionic-native/google-maps';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Latest',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Categories',
      url: '/categories',
      icon: 'list'
    },
    {
      title: 'Login',
      url: '/login',
      icon: 'list'
    },
    {
      title: 'Información',
      url: '/informacion',
      icon: 'list'
    },
    {
      title: 'Rutas',
      url: '/rutas',
      icon: 'list'
    },
    {
      title: 'Mapas',
      url: '/mapas',
      icon: 'list'
    },
    {
      title: 'Sitios',
      url: '/sitios',
      icon: 'list'
    }
  ];

  constructor(private platform: Platform,
              private splashScreen: SplashScreen,
              private statusBar: StatusBar,
              private authenticationService: AuthenticationService,
              private route: ActivatedRoute,
              public router: Router
            ) {
    this.initializeApp();
  }

  initializeApp() {
    this.authenticationService.getUser()
    .then(
      data => {
        this.authenticationService.validateAuthToken(data.token)
        .subscribe(
          res => this.router.navigateByUrl('/home'),
          err => this.router.navigateByUrl('/login')
        );
      },
      err => this.router.navigateByUrl('/login')
    );
    // Okay, so the platform is ready and our plugins are available.
    // Here you can do any higher level native things you might need.
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      Environment.setEnv({
        API_KEY_FOR_BROWSER_RELEASE: "AIzaSyCWmSvmAYkMVy1qyiNTC5OWUPQJHR1uhLI",
        API_KEY_FOR_BROWSER_DEBUG: "AIzaSyCWmSvmAYkMVy1qyiNTC5OWUPQJHR1uhLI"
      });

    });
  }
}
